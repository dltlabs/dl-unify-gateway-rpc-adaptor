FROM nginx:1.21

RUN rm /etc/nginx/conf.d/* \
    && rm /etc/nginx/nginx.conf \
    && mkdir -p /opt/docker/nginx/conf.d \
    && mkdir -p /opt/docker/nginx/cache \
    && mkdir -p /opt/docker/nginx/templates \
    && mkdir -p /opt/docker/tmp    
ADD conf/nginx.conf /etc/nginx/
ADD templates/frontend.conf.template /opt/docker/nginx/templates/
# ADD modules/ngx_http_headers_more_filter_module.so /usr/share/nginx/modules/
# ADD modules/ngx_http_headers_more_filter_module.so /etc/nginx/modules/
RUN chown -R nginx:nginx /opt/docker \
    && chmod o+r /etc/nginx/nginx.conf
ENV DEBIAN_FRONTEND noninteractive

RUN apt update 

EXPOSE 8080
USER root
