
# timeout configuration
# sets a timeout during which a keep-alive client connection will stay open on the server side
keepalive_timeout ${KEEP_ALIVE_TIMEOUT};

# sets a timeout for transmitting a response to the client,  timeout is set only between 
# two successive write operations, not for the transmission of the whole response
send_timeout ${SEND_TIMEOUT};
# defines a timeout for establishing a connection with a proxied server
# it should be noted that this timeout cannot usually exceed 75 seconds
proxy_connect_timeout ${PROXY_CONNECT_TIMEOUT};
# sets a timeout for transmitting a request to the proxied server
proxy_send_timeout ${PROXY_SEND_TIMEOUT};
# defines a timeout for reading a response from the proxied server
proxy_read_timeout ${PROXY_READ_TIMEOUT};

# sets the maximum allowed size of the client request body
client_max_body_size 10m;

# re‑resolve names every 10 seconds
resolver ${NAME_SERVER} valid=100s ipv6=off;

server {
    listen       ${NGINX_PORT};
    
    subrequest_output_buffer_size 16k;
    error_log /var/log/nginx/error.log debug;

    location = /mainnet {
        proxy_set_header  Authorization $remote_user;
        proxy_set_header  Content-Type "application/json";
        proxy_set_header  Origin "";
        proxy_method      POST;
        proxy_set_body    "$request_body";
        proxy_pass        "https://eth-mainnet.dltlabs.com/api/3.3/";
    }

    location = /rinkeby {
        proxy_set_header  Authorization $remote_user;
        proxy_set_header  Content-Type "application/json";
        proxy_set_header  Origin "";
        proxy_method      POST;
        proxy_set_body    "$request_body";
        proxy_pass        "https://rinkeby.dltlabs.com/api/3.3/";
    }

    location = /dltestnet {
        proxy_set_header  Authorization $remote_user;
        proxy_set_header  Content-Type "application/json";
        proxy_set_header  Origin "";
        proxy_method      POST;
        proxy_set_body    "$request_body";
        proxy_pass        "https://dltestnet.dltlabs.com/api/3.3";
    }

    location = /ropsten {
        proxy_set_header  Authorization $remote_user;
        proxy_set_header  Content-Type "application/json";
        proxy_set_header  Origin "";
        proxy_method      POST;
        proxy_set_body    "$request_body";
        proxy_pass        "https://ropsten.dltlabs.com/api/3.3/";
    }

    location = /health {
        return 200 '{"status":"UP"}';
        add_header Content-Type application/json;
    }
    
    
    location = /error/401 {
       internal;
       js_content content401;       
    }

    # redirect server error pages to the static page /50x.html
    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   /usr/share/nginx/html;
    }
}
