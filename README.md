# Converting Web3-Provider call into DL Gateway compliant RPC call.

WEB3 URL Syntax to be user:

   https://<dl-gateway-project-secret>:<dltestnet>@dlrpc.dltlabs.com/dltestnet
   i.e.: http://979f3677-d471-465f-a34f-fb35fdd36c36:dltestnet@192.168.0.101:8080/dltestnet

This syntax is compliant with Metamask or other wallets/web3 providers.